Pod::Spec.new do |s|
  s.name             = "FreewheelLib"
  s.version          = "6.20"
  s.summary          = "Just Freewheel library"
  s.homepage         = "http://freewheel.tv/freewheel-publishers/"
  s.license          = {:type => 'MIT', :file => 'LICENSE.md'}
  s.author           = { "Plugins" => "plugins@nicepeopleatwork.com" }
  s.source           = { :git => "https://bitbucket.org/npaw/freewheellib.git", :tag => s.version }

  s.frameworks = 'UIKit', 'CoreGraphics', 'QuartzCore', 'CoreLocation', 'MessageUI', 'EventKit', 'CoreMedia', 'AVFoundation', 'WebKit', 'AdSupport'
  s.library = 'xml2'
  s.xcconfig = { 'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2' }

  s.platform     = :ios, '8.0'

  s.vendored_frameworks = "AdManager.framework"
end
